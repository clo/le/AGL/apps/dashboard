TARGET = dashboard
QT = quickcontrols2

SOURCES = main.cpp

RESOURCES += \
    dashboard.qrc \
    images/images.qrc

include(app.pri)
